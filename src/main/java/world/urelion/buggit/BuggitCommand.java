package world.urelion.buggit;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.command.*;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@link CommandExecutor} of the {@link PluginCommand} buggit
 *
 * @since 1.0.0
 */
@Slf4j
@Singleton(style = Style.HOLDER)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BuggitCommand
implements CommandExecutor, TabCompleter {
	/**
	 * {@link Singleton} instance of the {@link BuggitCommand}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final @NotNull BuggitCommand INSTANCE = new BuggitCommand();

	/**
	 * {@link Set} of all possible sub commands
	 *
	 * @since 1.0.0
	 */
	private static final Set<String> SUB_COMMANDS = new HashSet<>(Arrays.asList(
		"pull",
		"copy",
		"reload-all"
	));

	/**
	 * executes the command
	 *
	 * @param sender  the {@link CommandSender}, which fires the command
	 * @param command the {@link Command}, which was fired
	 * @param label   the alias, which was used to fire the command
	 * @param args    the arguments given by the command
	 * @return {@code true}, if the command was successfully executed
	 *
	 * @see CommandExecutor#onCommand(CommandSender, Command, String, String[])
	 *
	 * @since 1.0.0
	 */
	@Override
	public boolean onCommand(
		final @NotNull CommandSender sender,
		final @NotNull Command command,
		final @NotNull String label,
		final @NotNull String[] args
	) {
		// ToDO
		return false;
	}

	/**
	 * gives the valid sub commands by given input
	 *
	 * @param sender  the {@link CommandSender}, which fires the command
	 * @param command the {@link Command}, which was fired
	 * @param label   the alias, which was used to fire the command
	 * @param args    the arguments given by the command
	 * @return the {@link List} of valid sub commands
	 *
	 * @see TabCompleter#onTabComplete(CommandSender, Command, String, String[])
	 *
	 * @since 1.0.0
	 */
	@NotNull
	@Override
	public List<String> onTabComplete(
		final @NotNull CommandSender sender,
		final @NotNull Command command,
		final @NotNull String label,
		final @NotNull String[] args
	) {
		BuggitCommand.log.trace("Define sub command.");
		final @NotNull String subCommand;
		BuggitCommand.log.trace("Check if any sub command was given.");
		if (args.length < 1 || StringUtils.isEmpty(args[0])) {
			BuggitCommand.log.trace("Set empty sub command.");
			subCommand = "";
		} else {
			BuggitCommand.log.trace("Get sub command from arguments.");
			subCommand = args[0];
		}

		BuggitCommand.log.trace("Filter sub commands by given argument.");
		return SUB_COMMANDS.stream().filter(
			entry -> entry.startsWith(subCommand)
		).collect(Collectors.toList());
	}
}
