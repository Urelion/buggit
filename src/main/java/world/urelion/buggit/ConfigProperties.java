package world.urelion.buggit;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bukkit.configuration.file.FileConfiguration;
import org.eclipse.jgit.api.Git;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

/**
 * contains fix names or path of properties in the {@link FileConfiguration}
 *
 * @since 1.0.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigProperties {
	/**
	 * property name to set the {@link URL} of the Git repository
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String REPO_URL = "repoUrl";
	/**
	 * property name to define the {@link Git} branch,
	 * to use as the current configuration
	 *
	 * @since 1.0.0
	 */
	public static final @NotNull String BRANCH   = "branch";
}
