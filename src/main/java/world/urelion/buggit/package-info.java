/**
 * root package of the Buggit plugin
 *
 * @version 1.0.0
 * @since 1.0.0
 */
package world.urelion.buggit;
