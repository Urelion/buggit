package world.urelion.buggit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * used to manage the {@link Git} repository
 *
 * @since 1.0.0
 */
@Slf4j
@AllArgsConstructor
public final class GitManager {
	/**
	 * {@link URL} of the origin {@link Git} repository
	 *
	 * @since 1.0.0
	 */
	@Getter
	private final @NotNull @NonNull String repositoryUrl;
	/**
	 * branch to use for current configuration
	 *
	 * @since 1.0.0
	 */
	@Getter
	final private @NotNull @NonNull String branch;
	/**
	 * directory to store the local repository
	 *
	 * @since 1.0.0
	 */
	@Getter
	final private @NotNull @NonNull File   localDirectory;

	/**
	 * reset local {@link Git} {@link Repository} by re-clone it
	 *
	 * @throws IOException if the local {@link Repository} couldn't be reset
	 *
	 * @since 1.0.0
	 */
	private void resetLocalRepo() throws IOException {
		GitManager.log.trace("Get directory of the local repository.");
		final File localDir = this.getLocalDirectory();

		GitManager.log.trace("Check if local repository directory exist.");
		if (localDir.exists()) {
			try {
				GitManager.log.debug("Delete local repository directory.");
				FileUtils.deleteDirectory(localDir);
			} catch (IOException ex) {
				final @NotNull String errorMessage =
					"Couldn't remove existing local repository!";
				GitManager.log.error(errorMessage);
				throw new IOException(errorMessage, ex);
			}
		}

		try {
			final @NotNull String repoUrl = this.getRepositoryUrl();
			GitManager.log.debug("Clone Git repository from " + repoUrl);
			Git.cloneRepository().setURI(
				repoUrl
			).setDirectory(
				localDir
			).setBranch(
				this.getBranch()
			).call().close();
		} catch (GitAPIException ex) {
			final @NotNull String errorMessage =
				"Couldn't clone Git repository!";
			GitManager.log.error(errorMessage);
			throw new IOException(errorMessage, ex);
		}
	}

	/**
	 * update local {@link Git} {@link Repository}
	 * to latest version from remote
	 *
	 * @throws IOException if an error occurred
	 *                     on update the {@link Repository}
	 *
	 * @since 1.0.0
	 */
	public void updateLocalRepo()
	throws IOException {
		GitManager.log.trace("Check if local repository directory exist.");
		if (!(this.getLocalDirectory().exists())) {
			try {
				GitManager.log.debug("Initialize Git repository.");
				this.resetLocalRepo();
			} catch (IOException ex) {
				final @NotNull String errorMessage =
					"Couldn't initialize Git repository!";
				GitManager.log.error(errorMessage);
				throw new IOException(errorMessage, ex);
			}
			GitManager.log.debug("Git repository initialized.");
			return;
		}

		GitManager.log.trace("Define local Git repository.");
		final @NotNull Git git;
		try {
			GitManager.log.trace("Load local Git repository.");
			git = Git.open(this.getLocalDirectory());
		} catch (IOException ex) {
			final @NotNull String errorMessage =
				"Couldn't open local Git repository!";
			GitManager.log.error(errorMessage);
			throw new IOException(errorMessage, ex);
		}
		GitManager.log.trace("Get Git repository.");
		final @Nullable Repository repository = git.getRepository();

		GitManager.log.trace("Check if Git repository exist.");
		if (repository == null) {
			final @NotNull String errorMessage =
				"Virtual Git repository couldn't created!";
			GitManager.log.error(errorMessage);
			throw new IOException(errorMessage);
		}

		GitManager.log.trace("Get remote URL of local Git repository.");
		final @Nullable String remoteUrlLocal =
			repository.getConfig().getString(
				"remote",
				"origin",
				"url"
			);
		GitManager.log.trace("Compare Git remote URL with configuration.");
		final boolean remoteUrlIdentical = this.getRepositoryUrl().equals(
			remoteUrlLocal
		);
		GitManager.log.trace("Check if remote URL is right.");
		if (!remoteUrlIdentical) {
			GitManager.log.trace("Close Git repository.");
			repository.close();
			GitManager.log.trace("Close Git.");
			git.close();
			try {
				GitManager.log.debug("Reset local Git repository.");
				this.resetLocalRepo();
			} catch (IOException ex) {
				final @NotNull String errorMessage =
					"Couldn't reset local Git repository!";
				GitManager.log.error(errorMessage);
				throw new IOException(errorMessage, ex);
			}
			GitManager.log.debug("Local Git repository reset.");
			return;
		}

		try {
			GitManager.log.trace("Pull Git changes from origin.");
			git.pull().call();
		} catch (GitAPIException ex) {
			GitManager.log.trace("Close Git repository.");
			repository.close();
			GitManager.log.trace("Close Git.");
			git.close();
			final @NotNull String errorMessage =
				"Couldn't pull changes from remote repository!";
			GitManager.log.error(errorMessage);
			throw new IOException(errorMessage, ex);
		}

		GitManager.log.trace("Define local branch.");
		final @Nullable String branchLocal;
		try {
			GitManager.log.trace("Get local branch.");
			branchLocal = repository.getBranch();
		} catch (IOException ex) {
			GitManager.log.trace("Close Git repository.");
			repository.close();
			GitManager.log.trace("Close Git.");
			git.close();
			final @NotNull String errorMessage =
				"Couldn't get current used branch!";
			GitManager.log.error(errorMessage);
			throw new IOException(errorMessage, ex);
		}
		GitManager.log.trace("Get branch from configuration.");
		final @NotNull String branchConfig = this.getBranch();
		GitManager.log.trace("Compare Git remote URL with configuration.");
		final boolean branchIdentical = branchConfig.equals(branchLocal);
		GitManager.log.trace("Check if branch is right.");
		if (!branchIdentical) {
			GitManager.log.trace("Define list of branches.");
			final List<Ref> branchList;
			try {
				GitManager.log.trace("Get list of branches.");
				branchList = git.branchList().call();
			} catch (GitAPIException ex) {
				GitManager.log.trace("Close Git repository.");
				repository.close();
				GitManager.log.trace("Close Git.");
				git.close();
				final @NotNull String errorMessage =
					"Couldn't get list of branches.";
				GitManager.log.error(errorMessage);
				throw new IOException(errorMessage, ex);
			}
			GitManager.log.trace("Iterate over all branches.");
			for (Ref branch : branchList) {
				GitManager.log.trace("Get branch name from list item.");
				final @NotNull String branchName = branch.getName();
				GitManager.log.trace("Check if branch is a local one.");
				if (!(branchName.startsWith("origin/"))) {
					try {
						GitManager.log.trace(
							"Delete local branch " + branchName + "."
						);
						git.branchDelete().setBranchNames(branchName).call();
					} catch (GitAPIException ex) {
						GitManager.log.trace("Close Git repository.");
						repository.close();
						GitManager.log.trace("Close Git.");
						git.close();
						final @NotNull String errorMessage =
							"Couldn't delete local branch " + branchName + "!";
						GitManager.log.error(errorMessage);
						throw new IOException(errorMessage, ex);
					}
				}
			}

			try {
				GitManager.log.debug(
					"Check out remote branch " + branchConfig + "."
				);
				git.checkout().setCreateBranch(true).setName(
					branchConfig
				).setUpstreamMode(SetupUpstreamMode.TRACK).setStartPoint(
					"origin/" + branchConfig
				).call();
			} catch (GitAPIException ex) {
				GitManager.log.trace("Close Git repository.");
				repository.close();
				GitManager.log.trace("Close Git.");
				git.close();
				final @NotNull String errorMessage =
					"Couldn't checkout right branch " + branchConfig +
					" from remote repository!";
				GitManager.log.error(errorMessage);
				throw new IOException(errorMessage, ex);
			}
		}

		GitManager.log.trace("Close Git repository.");
		repository.close();
		GitManager.log.trace("Close Git.");
		git.close();
	}
}
